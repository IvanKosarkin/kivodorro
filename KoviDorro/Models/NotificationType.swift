//
//  NotificationType.swift
//  KoviDorro
//
//  Created by KosarkinIvan on 27.01.2020.
//  Copyright © 2020 KosarkinIvan. All rights reserved.
//

import Foundation

enum NotificationType {
    case shortTime
    case workTime
    case longTime
}
