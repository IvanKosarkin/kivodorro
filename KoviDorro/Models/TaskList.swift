//
//  TaskList.swift
//  KoviDorro
//
//  Created by KosarkinIvan on 13.01.2020.
//  Copyright © 2020 KosarkinIvan. All rights reserved.
//

import Foundation

enum TaskList {
    case yesYes
    case yesNo
    case noYes
    case noNo
}
